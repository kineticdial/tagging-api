require 'rails_helper'

RSpec.describe TagsController, type: :controller do
  describe "POST /tag" do
    it "creates both entity and tags" do
      body = {
        tag: {
          entity_type: "Article",
          entity_public_id: "abc123",
          tags: ["Large", "Pink", "Bike"]
        }
      }

      post :create, params: body

      entity = Entity.find_by(public_id: "abc123")
      expect(entity.tags.map(&:name)).to eq(["Large", "Pink", "Bike"])
      expect(entity.entity_type).to eq("Article")
      expect(response).to be_success
    end

    context "updating an existing entity" do
      before do
        @entity = Entity.create(
          entity_type: "Article",
          public_id: "abc123",
          tags: [Tag.new(name: "Pink")]
        )
      end

      it "replaces all tags (doesn't append)" do
        body = {
          tag: {
            entity_type: "Article",
            entity_public_id: "abc123",
            tags: ["Large"]
          }
        }

        post :create, params: body

        @entity.reload
        expect(@entity.tags.map(&:name)).to eq(["Large"])
        expect(response).to be_success
      end

      it "destroys unused tags" do
        body = {
          tag: {
            entity_type: "Article",
            entity_public_id: "abc123",
            tags: ["Large"]
          }
        }

        post :create, params: body

        tag = Tag.find_by(name: "Pink")
        expect(tag).to be_nil
      end
    end
  end
end
