require 'rails_helper'

RSpec.describe EntitiesController, type: :controller do
  before do
    @entity = Entity.create(
      entity_type: "Article",
      public_id: "abc123",
      tags: [Tag.new(name: "Pink"), Tag.new(name: "Large")]
    )
  end

  describe "GET /entities/:entity_type/:entity_id" do
    it "returns a json representation of the entity and associated tags" do
      get :show, params: { entity_type: "Article", public_id: "abc123" }

      expect(response).to be_success
      expect(response.body).to eq({
        public_id: @entity.public_id,
        entity_type: @entity.entity_type,
        tags: @entity.tags.map(&:name),
      }.to_json)
    end

    it "returns a 404 when the entity is not found" do
      get :show, params: { entity_type: "Article", public_id: "xyz987" }
      expect(response.status).to be(404)
    end

    it "returns a 404 when the entity type does not exist" do
      get :show, params: { entity_type: "Book", public_id: "abc123" }
      expect(response.status).to be(404)
    end
  end

  describe "DELETE /entities/:entity_type/:entity_id" do
    it "deletes the entity by public_id and type" do
      delete :destroy, params: { entity_type: "Article", public_id: "abc123" }
      expect(response).to be_success
      expect(Entity.find_by(entity_type: "Article", public_id: "abc123")).to be_nil
    end

    it "deletes associated tags" do
      delete :destroy, params: { entity_type: "Article", public_id: "abc123" }
      expect(Tag.find_by(name: "Pink")).to be_nil
      expect(Tag.find_by(name: "Large")).to be_nil
    end

    it "leaves tags if they're associated with another entity" do
      Entity.create(
        entity_type: "Article",
        public_id: "xyz987",
        tags: [Tag.new(name: "Large")]
      )

      delete :destroy, params: { entity_type: "Article", public_id: "abc123" }
      expect(Tag.find_by(name: "Pink")).to be_nil
      expect(Tag.find_by(name: "Large")).not_to be_nil
    end
  end
end
