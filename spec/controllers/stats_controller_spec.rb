require 'rails_helper'

RSpec.describe StatsController, type: :controller do
  before do
    Entity.create(
      entity_type: "Article",
      public_id: "abc123",
      tags: [
        Tag.find_or_create_by(name: "Pink"),
        Tag.find_or_create_by(name: "Bike"),
      ]
    )
    Entity.create(
      entity_type: "Article",
      public_id: "xyz987",
      tags: [
        Tag.find_or_create_by(name: "Green"),
        Tag.find_or_create_by(name: "Bike"),
      ]
    )
    Entity.create(
      entity_type: "Book",
      public_id: "abc123",
      tags: [
        Tag.find_or_create_by(name: "Bike"),
        Tag.find_or_create_by(name: "Green"),
      ]
    )
  end

  describe "GET /stats/tags" do
    it "returns all stats for tags" do
      get :tags

      expect(response.body).to eq([
        {tag: "Bike", count: 3},
        {tag: "Green", count: 2},
        {tag: "Pink", count: 1}
      ].to_json)
      expect(response).to be_success
    end
  end

  describe "GET /stats/entities" do
    it "returns all stats for tags" do
      get :entities

      expect(response.body).to eq([
        {entity_type: "Article", count: 2},
        {entity_type: "Book", count: 1},
      ].to_json)
      expect(response).to be_success
    end
  end
end
