class TagCreationService
  def initialize(entity, tags)
    @entity = entity
    @tags = tags
  end

  def destroy_unused_tags
    @entity.tags
      .select { |t| !t.in?(@tags) }
      .each { |t| t.destroy }
  end

  def persist_tags_to_entity
    @entity.tags = @tags
    @entity.save!
  end
end
